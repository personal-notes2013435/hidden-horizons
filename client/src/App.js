// import logo from './logo.svg';
import './App.css';
import { Routes, Route } from 'react-router-dom';
import LandingPage from './screens/LandingPage';
import Layout from './screens/Layout';
import Aboutus from './screens/Aboutus';
import Mission from './screens/Mission';
import Vision from './screens/Vision';
import Values from './screens/Values';
import Destinations from './screens/Destinations';
import DDetails from './screens/DDetails';
import Download from './screens/Download';
import Signup from './screens/Signup';
import Login from './screens/Login';
import Forgotpassword from './screens/Forgotpassword';

// This import are for pages after login

import HomePage from './screens/AfterLogin/Homepage';
import DestinationsA from './screens/AfterLogin/DestinationsA';
import PlanTrip from './screens/AfterLogin/PlanTrip';
import Community from './screens/AfterLogin/Community';
function App() {
  return (
    <div className="App" >
    <Routes>
    <Route path="/" element={<LandingPage/>}/>
    <Route path="/aboutus" element={<Aboutus/>}/>
    <Route path="/mission" element={<Mission/>}/>
    <Route path="/vision" element={<Vision/>}/>
    <Route path="/values" element={<Values/>}/>
    <Route path="/destinations" element={<Destinations/>}/>
    <Route path="/details" element={<DDetails/>}/>
    <Route path="/download" element={<Download/>}/>
    <Route path="/signup" element={<Signup/>}/>
    <Route path="/login" element={<Login/>}/>
    <Route path="/ForgotPassword" element={<Forgotpassword/>}/>

    {/* This route are for pages after login */}

    <Route path="/home" element={<HomePage/>}/>
    <Route path='/destinationsa' element={<DestinationsA/>}/>
    <Route path='/plantrip' element={<PlanTrip/>}/>
    <Route path='/community' element={<Community/>}/>

  {/* Optional Route */}
    <Route path="/nav" element={<Layout/>}/>
  </Routes>

  </div>
  );
}

export default App;
