import React from 'react'
import Layout from './Layout'
import Footer from './Footer'
import { FaArrowLeft } from 'react-icons/fa'
import { Link } from 'react-router-dom'
function DDetails() {
  return (
    <>
    <Layout/>

    <div className='row '>
        <div className='col-lg-1 col-md-1 col-sm-12'>
        <a href='/destinations' style={{margin:5, marginRight:15}}><FaArrowLeft/></a>
        </div>
        <div className='col-lg-6 col-md-10 col-sm-12 text-left '>
        <h3>Destination Name</h3>
        
         <p className='text'>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Viverra ipsum nunc aliquet bibendum enim facilisis gravida neque convallis. Faucibus a pellentesque sit amet porttitor eget dolor morbi. Elit pellentesque habitant morbi tristique senectus et netus. Sagittis vitae et leo duis ut. Non blandit massa enim nec. Sed risus ultricies tristique nulla aliquet enim tortor at. Turpis cursus in hac habitasse platea dictumst. Neque gravida in fermentum et sollicitudin ac orci phasellus egestas. Eu turpis egestas pretium aenean pharetra magna. Eget lorem dolor sed viverra ipsum nunc aliquet bibendum enim. Adipiscing at in tellus integer feugiat scelerisque varius morbi enim. Bibendum arcu vitae elementum curabitur vitae nunc. Pellentesque elit eget gravida cum sociis natoque penatibus et magnis. Est placerat in egestas erat imperdiet sed euismod nisi porta. Urna et pharetra pharetra massa massa ultricies. Pulvinar mattis nunc sed blandit libero volutpat. Malesuada pellentesque elit eget gravida cum. Purus faucibus ornare suspendisse sed nisi.

            Faucibus in ornare quam viverra orci. Quis vel eros donec ac odio tempor orci. Enim neque volutpat ac tincidunt vitae semper quis lectus. Massa ultricies mi quis hendrerit dolor magna. Faucibus pulvinar elementum integer enim neque volutpat ac tincidunt. Augue eget arcu dictum varius duis at. Aliquet sagittis id consectetur purus. Arcu ac tortor dignissim convallis. Ultrices neque ornare aenean euismod elementum nisi quis. Consectetur adipiscing elit duis tristique sollicitudin. Vehicula ipsum a arcu cursus vitae congue mauris rhoncus. Vulputate odio ut enim blandit volutpat maecenas. Bibendum est ultricies integer quis auctor elit sed vulputate mi. Sagittis id consectetur purus ut faucibus pulvinar. 
        </p>
        </div>

        <div className="col-lg-5 col-md-12 col-sm-12">
          <div className="container-flex border">
            <h4>Reviews</h4>
            <p>
              {/* Reviews content */}
            </p>
            <Link to="/signup">
              Read More <i className="arrow right"></i>
            </Link>
          </div>
          <div className="container-flex border">
            <h4>Images</h4>
            <p>
              {/* Images content */}
            </p>
            <Link to="/signup">
              View More <i className="arrow right"></i>
            </Link>
          </div>
        </div>
    </div>
    <Footer/>
    </>
  )
}

export default DDetails