import React from 'react';
import Layout from './Layout';
import Footer from './Footer';
import {Link} from 'react-router-dom';
// import taktshang from '../images/taktshang.jpg';
// import snowcaped from '../images/snowcaped.jpg';

const captionStyle = {
  position: 'absolute',
  top: 0,
  left: 0,
  width: '100%',
  height: '100%',
  display: 'flex',
  flexFlow: 'column',
  justifyContent: 'center',
  alignItems: 'center',
  padding: '20px',
  background: 'rgba(0, 0, 0, 0.5)',
  color: 'white',
};

const buttonStyle = {
  marginTop: '20px',
};

const carouselItemStyle = {
  borderRadius: '50px',
};

function LandingPage() {
  return (
    <>
      <Layout />

      <div className="container">
        <div
          id="carouselExampleCaptions"
          className="carousel slide"
          data-bs-ride="carousel" // Add data-bs-ride for auto-play
          style={{
            width: '100%',
            height: 'auto',
            margin: '0 auto',
            marginTop: 20,
            position: 'relative',
          }}
        >
          <div className="carousel-indicators">
            <button
              type="button"
              data-bs-target="#carouselExampleCaptions"
              data-bs-slide-to="0"
              className="active"
              aria-current="true"
              aria-label="Slide 1"
            />
            <button
              type="button"
              data-bs-target="#carouselExampleCaptions"
              data-bs-slide-to="1"
              aria-label="Slide 2"
            />
            <button
              type="button"
              data-bs-target="#carouselExampleCaptions"
              data-bs-slide-to="2"
              aria-label="Slide 3"
            />
          </div>
          <div className="carousel-inner">
            <div className="carousel-item active" style={carouselItemStyle}>
              <img
                src="/taktshang.jpg"
                className="d-block w-100"
                alt="..."
                style={{ objectFit: 'cover', width: '100%', height: 'auto', borderRadius: '50px' }}
              />
              <div style={captionStyle}>
                <h1 className="display-4 font-weight-bold">
                  Discover the Unseen,
                  <br />
                  Explore the Unforgettable
                  <br />
                  with Hidden Horizons.
                </h1>
                <Link to="/signup" className="btn btn-primary btn-lg" style={buttonStyle}>
                  Explore Now
                </Link>
              </div>
            </div>
            <div className="carousel-item" style={carouselItemStyle}>
              <img
                 src="/taktshang.jpg"
                className="d-block w-100"
                alt="..."
                style={{ objectFit: 'cover', width: '100%', height: 'auto', borderRadius: '50px' }}
              />
              <div style={captionStyle}>
                <h1 className="display-4 font-weight-bold">
                  Discover the Unseen,
                  <br />
                  Explore the Unforgettable
                  <br />
                  with Hidden Horizons.
                </h1>
                <Link to="/signup" className="btn btn-primary btn-lg" style={buttonStyle}>
                  Explore Now
                </Link>
              </div>
            </div>
            <div className="carousel-item" style={carouselItemStyle}>
              <img
                src="/snowcaped.jpg"
                className="d-block w-100"
                alt="..."
                style={{ objectFit: 'cover', width: '100%', height: 'auto', borderRadius: '50px' }}
              />
              <div style={captionStyle}>
                <h1 className="display-4 font-weight-bold">
                  Discover the Unseen,
                  <br />
                  Explore the Unforgettable
                  <br />
                  with Hidden Horizons.
                </h1>
                <Link to="/signup" className="btn btn-primary btn-lg" style={buttonStyle}>
                  Explore Now
                </Link>
              </div>
            </div>
          </div>
          <button
            className="carousel-control-prev"
            type="button"
            data-bs-target="#carouselExampleCaptions"
            data-bs-slide="prev"
          >
            <span className="carousel-control-prev-icon" aria-hidden="true" style={{ fontSize: '2rem' }}></span> {/* Increase arrow size */}
            <span className="visually-hidden">Previous</span>
          </button>
          <button
            className="carousel-control-next"
            type="button"
            data-bs-target="#carouselExampleCaptions"
            data-bs-slide="next"
          >
            <span className="carousel-control-next-icon" aria-hidden="true" style={{ fontSize: '2rem' }}></span> {/* Increase arrow size */}
            <span className="visually-hidden">Next</span>
          </button>
        </div>
      </div>
      <Footer />
    </>
  );
}

export default LandingPage;
