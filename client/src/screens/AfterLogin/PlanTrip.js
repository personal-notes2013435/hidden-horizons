import React from 'react'
import NavAfter from './LayoutAfter'
import FooterAfter from './FooterAfter'

function PlanTrip() {
  return (
    <>
        <NavAfter/>
        <div className='row justify-content-start'>
        <div className='col-lg-4 col-md-6 col-sm-12 d-flex flex-row '>
      <div className="dropdown" style={{ marginRight: '10px' }}>
        <button className="btn btn-secondary dropdown-toggle" style={{marginLeft:'5px'}} type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
          Country
        </button>
        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
          <li><a className="dropdown-item" href="#">Option 1</a></li>
          <li><a className="dropdown-item" href="#">Option 2</a></li>
          <li><a className="dropdown-item" href="#">Option 3</a></li>
        </ul>
      </div>

      <div className="dropdown" style={{ marginRight: '10px' }}>
        <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton2" data-bs-toggle="dropdown" aria-expanded="false">
            District
        </button>
        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton2">
          <li><a className="dropdown-item" href="#">Option 1</a></li>
          <li><a className="dropdown-item" href="#">Option 2</a></li>
          <li><a className="dropdown-item" href="#">Option 3</a></li>
        </ul>
      </div>

      <div className="dropdown">
        <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton3" data-bs-toggle="dropdown" aria-expanded="false">
          Destination
        </button>
        <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton3">
          <li><a className="dropdown-item" href="#">Option 1</a></li>
          <li><a className="dropdown-item" href="#">Option 2</a></li>
          <li><a className="dropdown-item" href="#">Option 3</a></li>
        </ul>
      </div>
    </div>
        </div>
        
        <div className='row mt-3'>
            <div className='col-lg-3 col-md-6 col-sm-12 justify-content-center d-flex flex-column column-left border  rounded'>
                <h4>
                    Things to Do
                </h4>
                <hr/>
                <div className='text-left'>
                    <input type='checkbox'/>
                    <span>Do a dance and record a video</span>
                </div>
            </div>

            <div className='col-lg-3 col-md-6 col-sm-12 justify-content-center d-flex flex-column column-left border  rounded'>
                <h4>
                    Things To Take
                </h4>
                <hr/>
                <div className='text-left'>
                    <input type='checkbox'/>
                    <span>Do a dance and record a video</span>
                </div>
            </div>

            <div className='col-lg-3 col-md-6 col-sm-12 justify-content-center d-flex flex-column column-left border  rounded'>
                <h4>
                    Budget Planner
                </h4>
                <hr/>
                <div className='text-left'>
                    <input type='checkbox'/>
                    <span>Do a dance and record a video</span>
                </div>
            </div>

            <div className='col-lg-3 col-md-6 col-sm-12 justify-content-center d-flex flex-column column-left border rounded'>
                <p className='h4 m-2'>
                    Important
                </p>
                <hr/>
                <div className='text-left'>
                    <p>
                    <a href='#'>
                        Map
                    </a>
                    </p>
                   <p>
                   <input type='checkbox'/>
                    <span>Do a dance and record a video</span>
                   </p>
                </div>
            </div>
        </div>

        <FooterAfter/>
    </>
  )
}

export default PlanTrip