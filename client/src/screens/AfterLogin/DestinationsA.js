import React from 'react';
import NavAfter from './LayoutAfter';
import FooterAfter from './FooterAfter';
// import snowcaped from '../../images/snowcaped.jpg';


import '../Destinations.css';

const DestinationsA = () => {
    
  return (
    <>
      <NavAfter />
      <div className="row justify-content-end">
          <div className="col-lg-4 col-md-6 col-sm-12 d-flex">
            <div className="input-group mb-3">
              {/* Dropdown */}
              <select className="form-select">
                <option value="option1">All</option>
                <option value="option2">New Added</option>
                <option value="option3">Less Visted</option>
              </select>

              {/* Search Bar */}
              <input
                type="text"
                className="form-control"
                placeholder="Search..."
                aria-label="Search"
                aria-describedby="basic-addon2"
              />

              {/* Search Button */}
              <button className="btn btn-primary" type="button">
                Search
              </button>
            </div>
          </div>
        </div>

      <div className="container-fluid">
      <a href='/details'>
      <div className="row justify-content-center">
          <div className="col-lg-3 col-md-4 col-sm-6 m-3" >
            <div className="image-container" style={{ position: 'relative' }}>
              <img
                src="/snowcaped.jpg"
                className="d-block w-100"
                alt="..."
                style={{ borderRadius: '10px' }}
              />
              <div className="destination-info">
                <h5>Destination Name</h5>
                <p>Description</p>
              </div>
            </div>
          </div>
       
          <div className="col-lg-3 col-md-4 col-sm-6 m-3">
            <div className="image-container" style={{ position: 'relative' }}>
              <img
                src="/snowcaped.jpg"
                className="d-block w-100"
                alt="..."
                style={{ borderRadius: '10px' }}
              />
              <div className="destination-info">
                <h5>Destination Name</h5>
                <p>Description</p>
              </div>
            </div>
          </div>
        
          <div className="col-lg-3 col-md-4 col-sm-6 m-3">
            <div className="image-container" style={{ position: 'relative' }}>
              <img
                src="/snowcaped.jpg"
                className="d-block w-100"
                alt="..."
                style={{ borderRadius: '10px' }}
              />
              <div className="destination-info">
                <h5>Destination Name</h5>
                <p>Description</p>
              </div>
            </div>
          </div>
        </div>
      </a>

        <div className="row justify-content-center">
          <div className="col-lg-3 col-md-4 col-sm-6 m-3">
            <div className="image-container" style={{ position: 'relative' }}>
              <img
                src="/snowcaped.jpg"
                className="d-block w-100"
                alt="..."
                style={{ borderRadius: '10px' }}
              />
              <div className="destination-info">
                <h5>Destination Name</h5>
                <p>Description</p>
              </div>
            </div>
          </div>
       
          <div className="col-lg-3 col-md-4 col-sm-6 m-3">
            <div className="image-container" style={{ position: 'relative' }}>
              <img
                src="/snowcaped"
                className="d-block w-100"
                alt="..."
                style={{ borderRadius: '10px' }}
              />
              <div className="destination-info">
                <h5>Destination Name</h5>
                <p>Description</p>
              </div>
            </div>
          </div>
        
          <div className="col-lg-3 col-md-4 col-sm-6 m-3">
            <div className="image-container" style={{ position: 'relative' }}>
              <img
                src="/snowcaped.jpg"
                className="d-block w-100"
                alt="..."
                style={{ borderRadius: '10px' }}
              />
              <div className="destination-info">
                <h5>Destination Name</h5>
                <p>Description</p>
              </div>
            </div>
          </div>
        </div>

        <div className="row justify-content-center">
          <div className="col-lg-3 col-md-4 col-sm-6 m-3">
            <div className="image-container" style={{ position: 'relative' }}>
              <img
                src="/snowcaped.jpg"
                className="d-block w-100"
                alt="..."
                style={{ borderRadius: '10px' }}
              />
              <div className="destination-info">
                <h5>Destination Name</h5>
                <p>Description</p>
              </div>
            </div>
          </div>
       
          <div className="col-lg-3 col-md-4 col-sm-6 m-3">
            <div className="image-container" style={{ position: 'relative' }}>
              <img
                src="/snowcaped.jpg"
                className="d-block w-100"
                alt="..."
                style={{ borderRadius: '10px' }}
              />
              <div className="destination-info">
                <h5>Destination Name</h5>
                <p>Description</p>
              </div>
            </div>
          </div>
        
          <div className="col-lg-3 col-md-4 col-sm-6 m-3">
            <div className="image-container" style={{ position: 'relative' }}>
              <img
                src="/snowcaped.jpg"
                className="d-block w-100"
                alt="..."
                style={{ borderRadius: '10px' }}
              />
              <div className="destination-info">
                <h5>Destination Name</h5>
                <p>Description</p>
              </div>
            </div>
          </div>
        </div>
        
      </div>
      <FooterAfter />
    </>
  );
};

export default DestinationsA;
