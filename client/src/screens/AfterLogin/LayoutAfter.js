import React from 'react';
// import logo from '../../images/logo.svg';
import {Link} from 'react-router-dom';

function NavAfter() {
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <div className="container">
        <a className="navbar-brand me-auto" href="/">
          <img className="img-fluid mt-3" src="/logo.svg" alt="logo" />
        </a>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarNav"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav ms-auto">
            <li className="nav-item">
              <Link to="/home" className="nav-link active" aria-current="page">
                HOME
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/destinationsa" className="nav-link">
                DESTINATIONS
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/plantrip" className="nav-link">
                PLAN TRIP
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/community" className="nav-link">
               COMMUNITY
              </Link>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default NavAfter;
