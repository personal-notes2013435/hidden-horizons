import React from 'react'
import FooterAfter from './FooterAfter'
import NavAfter from './LayoutAfter'
import {FaUser} from 'react-icons/fa'

function Community() {
  return (
    <>
    <NavAfter/>

    <div className="row justify-content-center">
  <div className="col-4 d-flex">
 
    <input type="text" placeholder="Search..." className="form-control form-control-sm" />
   
    <button className="btn btn-primary  btn-sm">Search</button>
  </div>
  <div className="col-3">
 
    <button className="btn btn-success btn-sm">Post Question</button>
  </div>
</div>

<div className='row justify-content-center'>
             <div className='col-lg-8 col-md-8 col-sm-10 justify-content-center d-flex flex-column column-left border rounded'>
                    <div className='container-fluid d-flex text-left'>
                    <a href='#'> <FaUser/></a>
                    <p className='align-self-start m-3'>When is the best time to visit kerala?</p>  
                    </div>
            </div>
          
</div>

    <FooterAfter/>
    </>
  )
}

export default Community