import React from 'react';
import Footer from './Footer';
import Layout from './Layout';
// import phone from '../images/phone.png';

function Download() {
  return (
    <>
      <Layout />
      <div className="row">
        <div className="col-lg-6 col-md-6 col-sm-10 d-flex align-items-center">
          <div>
            <h1>DOWNLOAD</h1>
            <h4>OUR APP</h4>
            <h6>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Viverra ipsum nunc aliquet bibendum enim facilisis gravida neque convallis.
            </h6>
            <button className="btn btn-primary">DOWNLOAD</button>
          </div>
        </div>

        <div className="col-lg-6 col-md-6 col-sm-10">
          <img src="/phone.png" alt="Phone" className="img-fluid" />
        </div>
      </div>
      <Footer />
    </>
  );
}

export default Download;
