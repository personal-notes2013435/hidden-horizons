import React from 'react'
// import logo from '../images/logo.svg'
import Footer from './Footer'
import { Link } from 'react-router-dom'

function Login() {
  return (
    <>
    <div className='row align-self-top'>
        <div className='col-2 align-self-start'>
        <a className="navbar-brand me-auto" href="/">
          <img className="img-fluid mt-3" src="/logo.svg" alt="logo" />
        </a>
        </div>
    </div>
    <div className='row align-self-center justify-content-center'>
            <h2>Login</h2>
            <div className='col-lg-3 col-md-6 col-sm-8 bg-secondary text-white mb-2 ' style={{borderRadius:10}}>
                   
                    <input className='form-control mt-4' type='text' placeholder='Name'/>
                    <input className='form-control mt-4' type='text' placeholder='Password'/>
                    <a href='/ForgotPassword' className='text mt-3' style={{textDecoration:'none', color:'white'}}>
                        Forgot Password
                    </a>
                    <br/>
                    {/* <button className='btn btn-primary mt-4'>
                            Login
                    </button> */}
                    <Link to='/home' className="btn btn-primary mt-4">
                        Login
                    </Link>
                    <br/>
                    
                    <input className='form-control mt-4 mb-4' type='button' value={'Continue with google?'} />
                    
            </div>
    </div>
    <Footer/>
    </>
  )
}

export default Login