import React from 'react'

import {FaFacebook, FaLinkedin} from'react-icons/fa'
import {RiInstagramFill} from'react-icons/ri'
function Footer() {
  return (

    <div className='container border' style={{display:'flex', flexWrap:'wrap'}}>
        <div style={{flexGrow:1, flexBasis:200, marginTop:10, display:'flex', flexDirection:'column'}}>
            <h6 style={{fontSize:'small'}}>QUICK TAB</h6>
            <a href='#' style={{fontSize:'small'}}>Home</a>
            <a href='#' style={{fontSize:'small'}}>Destinations</a>
            <a href='#' style={{fontSize:'small'}}>Download</a>
        </div>
        <div style={{flexGrow:1, flexBasis:200, marginTop:10}}>
            <h6 style={{fontSize:'small'}}>FOLLOW US</h6>
            <FaFacebook style={{margin:5}}/>
            <RiInstagramFill style={{margin:5}}/>
            <FaLinkedin style={{margin:5}}/>
        </div>
        <div style={{flexGrow:1, flexBasis:200, marginTop:10}}>
            <h6 style={{fontSize:'small'}}>CONTACT US</h6>
            <p style={{fontSize:'small'}}>hiddenhorizons@gmail.com</p>
            <p style={{fontSize:'small'}}>17944697</p>
        </div>
    </div>
  )
}

export default Footer