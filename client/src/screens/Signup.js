import React from 'react'
// import logo from '../images/logo.svg'
import Footer from './Footer'
import {Link} from 'react-router-dom';

function Signup() {
  return (
    <>
    <div className='row align-self-top'>
        <div className='col-2 align-self-start'>
        <a className="navbar-brand me-auto" href="/">
          <img className="img-fluid mt-3" src="/logo.svg" alt="logo" />
        </a>
        </div>
    </div>
    <div className='row align-self-center justify-content-center'>
            <h2>Signup</h2>
            <div className='col-lg-3 col-md-6 col-sm-8 bg-secondary text-white mb-2 ' style={{borderRadius:10}}>
                   
                    <input className='form-control mt-4' type='text' placeholder='Name'/>
                    <input className='form-control mt-4' type='text' placeholder='Email'/>
                    <input className='form-control mt-4' type='text' placeholder='Phone Number'/>
                    <input className='form-control mt-4' type='text' placeholder='Password'/>
                    <Link to="/login">
                    <button className='btn btn-primary mt-4'>
                            Signup
                    </button>
                    </Link>
                    <br/>
                    <Link to="/login" className='text mt-2' style={{textDecoration:'none', color:'white'}}>
                        Already have an account?
                    </Link>
                    
                    <input className='form-control mt-4 mb-4' type='button' value={'Continue with google?'} />
                    
            </div>
    </div>
    <Footer/>
    </>
  )
}

export default Signup