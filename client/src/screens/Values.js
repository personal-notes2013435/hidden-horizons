import React from 'react'
import Layout from './Layout'
import Footer from './Footer'

function Values() {

    const astyle={
        textDecoration: 'none',
        margin:10,
        color:'white'
    }
  return (
    <>
    <Layout/>
    <div className="container">
  <div className="row">
    <div className="col-lg-9 col-md-12 col-sm-12 text-left">
      <h3>Values</h3>
      <p className='text'>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Viverra ipsum nunc aliquet bibendum enim facilisis gravida neque convallis. Faucibus a pellentesque sit amet porttitor eget dolor morbi. Elit pellentesque habitant morbi tristique senectus et netus. Sagittis vitae et leo duis ut. Non blandit massa enim nec. Sed risus ultricies tristique nulla aliquet enim tortor at. Turpis cursus in hac habitasse platea dictumst. Neque gravida in fermentum et sollicitudin ac orci phasellus egestas. Eu turpis egestas pretium aenean pharetra magna. Eget lorem dolor sed viverra ipsum nunc aliquet bibendum enim. Adipiscing at in tellus integer feugiat scelerisque varius morbi enim. Bibendum arcu vitae elementum curabitur vitae nunc. Pellentesque elit eget gravida cum sociis natoque penatibus et magnis. Est placerat in egestas erat imperdiet sed euismod nisi porta. Urna et pharetra pharetra massa massa ultricies. Pulvinar mattis nunc sed blandit libero volutpat. Malesuada pellentesque elit eget gravida cum. Purus faucibus ornare suspendisse sed nisi.

Faucibus in ornare quam viverra orci. Quis vel eros donec ac odio tempor orci. Enim neque volutpat ac tincidunt vitae semper quis lectus. Massa ultricies mi quis hendrerit dolor magna. Faucibus pulvinar elementum integer enim neque volutpat ac tincidunt. Augue eget arcu dictum varius duis at. Aliquet sagittis id consectetur purus. Arcu ac tortor dignissim convallis. Ultrices neque ornare aenean euismod elementum nisi quis. Consectetur adipiscing elit duis tristique sollicitudin. Vehicula ipsum a arcu cursus vitae congue mauris rhoncus. Vulputate odio ut enim blandit volutpat maecenas. Bibendum est ultricies integer quis auctor elit sed vulputate mi. Sagittis id consectetur purus ut faucibus pulvinar. 

      </p>
    </div>
    <div className="col-lg-3 col-md-12 col-sm-12 align-self-center">
    <div className="container d-lg-flex flex-lg-column d-md-flex flex-md-row d-sm-flex flex-sm-row justify-content-center">
        <button className='btn btn-success '><a href='/aboutus' style={astyle}>Overview</a></button>
        <button className='btn btn-success '>  <a href='/mission' style={astyle}>Mission</a></button>
        <button className='btn btn-success'><a href='/vision' style={astyle}>Vision</a></button>
        <button className='btn btn-success border border-white'> <a href='/values' style={astyle}>Values</a></button>
      </div>
    </div>
  </div>    
</div>

    <Footer/>
    </>
  )
}

export default Values