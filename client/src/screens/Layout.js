import React from 'react';
import {Link} from 'react-router-dom'
// import logo from '../images/logo.svg';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <div className="container">
        <a className="navbar-brand me-auto" href="/">
          <img className="img-fluid mt-3" src="/logo.svg" alt="logo" />
        </a>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarNav"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav ms-auto">
            <li className="nav-item">
              <Link to="/" className="nav-link">
                HOME
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/aboutus" className="nav-link" >
                ABOUT US
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/destinations" className="nav-link">
                DESTINATIONS
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/download" className="nav-link">
                DOWNLOAD
              </Link>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
