import React from 'react'
// import logo from '../images/logo.svg'
import Footer from './Footer'
import { FaArrowLeft } from 'react-icons/fa'

function Forgotpassword() {
  return (
    <>
    <div className='row align-self-top'>
        <div className='col-2 align-self-start'>
        <a className="navbar-brand me-auto" href="/">
          <img className="img-fluid mt-3" src="/logo.svg" alt="logo" />
        </a>
        </div>
    </div>
    <div className='row align-self-center justify-content-center'>
            <h2>Forgot Password</h2>
                
            
            <div className='col-lg-3 col-md-6 col-sm-8 bg-secondary text-white mb-2 ' style={{borderRadius:10}}>
                    <FaArrowLeft/>
                   <label className='label'>Please enter your registered email account to get the procedure to reset password. </label>
                    <input className='form-control mt-4' type='email' placeholder='Email'/>

                    <button className='btn btn-primary mt-4'>
                            Confirm
                    </button>
                  
                  
                    
            </div>
    </div>
    <Footer/>
    </>
  )
}

export default Forgotpassword